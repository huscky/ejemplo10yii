<?php


namespace app\models;

use yii\base\Model;

class Numeros extends Model{
    public $numero1;
    public $numero2;
    public $tipo;
    public $ejercicio1;
    public $ejercicio2;
    public $ejercicio3;
    public $ejercicio4;
    
    public function attributeLabels(){
        return[
            "numero1"=>"Numero 1",
            "numero2"=>"Numero 2",
            "ejercicio1"=> "la secuencia es",
            "ejercicio2"=> "el patron es ",
            "ejercicio3"=> "el numero3 es mayor que el numero2",
            "ejercicio4"=> "el numero3 es mayor que el numero2",
            
            
        ];
            
    }
    
    public function rules(){
        return [
            [['numero1','numero2'],'integer'],
            [['numero1','numero2'],'required'],
            
            []
          /*  ['numero2', 'compare', 'compareValue' => 0, 'operator' => '!=', 'type' => 'number','when'=>function($model){
            $model->tipo=="dividir"; //solo relaiza la validad si la operacion es dividir.
            }]*/
            
            
    ];
    }
    /**
     * esta regla me comprueba cuando estoy diviendo que el numero2  no sea 0
     * @param type $atributo
     * @param type $parametros
     */
    public function dividirPorCero($atributo,$parametros){
        if($this->tipo=="dividir" || $this->tipo=="todo"){
            if($this->numero2==0){
                $this->addError("numero2", "No puede ser un 0 si se divide");
                
            }
            
        }
    }
            
    public function operacion (){
        switch ($this->tipo){
            
            case 'ejercicio1':
                return $this -> numero1;
                break;
            case 'ejercicio2':
                return $this -> numero1-$this->numero2;
                break;
             case 'ejercicio3':
                return $this -> numero3>$this->numero2;
                break;
             case 'ejercicio4':
                return $this -> numero3*$this->numero4;
                break;
            default:
                return 0;
        }
        
        
    }
    
    public function operarTodo(){
        
        $this->ejercicio1=$this->numero1+$this->numero2;
        $this->ejercicio2=$this->numero1-$this->numero2;
        $this->ejercicio3=$this->numero1*$this->numero2;
        $this->ejercicio4=$this->numero1/$this->numero2;
        
    }
}
