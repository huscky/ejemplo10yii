<?php


//vista verP
use yii\grid\GridView;
use yii\helpers\Html;


echo GridView::widget([
    "dataProvider" => $dataprovider,
    "columns" => [
        
        'id',
        'Titulo',
        
       
        [
            'label'=>'foto',
            'format'=>'raw',
            'value' => function($data){
                $url = '@web/imgs/' . $data->Portada;
                return Html::img($url,['class'=>'img-fluid','style'=>'width:300px']); 
            }
        ],
                'Fecha_estreno',
       ]     
]);