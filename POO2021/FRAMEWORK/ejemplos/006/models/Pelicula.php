<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pelicula".
 *
 * @property int $id
 * @property string $Titulo
 * @property string $Descripcion
 * @property string $Duracion
 * @property string $Fecha_estreno
 * @property string $Categoría
 * @property string $Portada
 * @property string $Director
 * @property int $Destacadas
 * @property int $Pelicula_de_la_semana
 */
class Pelicula extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pelicula';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Titulo', 'Descripcion', 'Duracion', 'Fecha_estreno', 'Categoría', 'Portada', 'Director', 'Destacadas', 'Pelicula_de_la_semana'], 'required'],
            [['Duracion', 'Fecha_estreno'], 'safe'],
            [['Destacadas', 'Pelicula_de_la_semana'], 'integer'],
            [['Titulo', 'Descripcion', 'Portada', 'Director'], 'string', 'max' => 100],
            [['Categoría'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Titulo' => 'Titulo',
            'Descripcion' => 'Descripcion',
            'Duracion' => 'Duracion',
            'Fecha_estreno' => 'Fecha Estreno',
            'Categoría' => 'Categoría',
            'Portada' => 'Portada',
            'Director' => 'Director',
            'Destacadas' => 'Destacadas',
            'Pelicula_de_la_semana' => 'Pelicula De La Semana',
        ];
    }
}
