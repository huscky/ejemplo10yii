<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Noticias;
use app\models\Fotos;
use app\models\Comentarios;
use yii\helpers\Html;
use yii\web\UploadedFile;
use app\models\NoticiasFotos;



class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        
        
        
        $consulta=Noticias::find();
        $fotos=Fotos::find();// muestra todas las fotos de la base de datos.
        $dataProvider = new ActiveDataProvider([
            'query' =>$consulta,
            
            
        ]
    
   
);
return $this->render('index',['dataProvider'=> $dataProvider
        
        ]);

    }
    
    public function actionNuevocomentario($codigo){
           $model = new Comentarios();
           
          

    if ($model->load(Yii::$app->request->post())) {
        $model->cod_noticias=$codigo;
        if ($model->save()) {
            // form inputs are valid, do something here
            return $this->redirect(["vercomentarios","codigo"=>$codigo]);
        }
    }

    return $this->render('formulario', [
        'model' => $model,
    ]);
}
        
        
    
    public function actionVercomentarios($codigo){
        
        $consulta= Comentarios::find()->where(["cod_noticias"=>$codigo]);
        $noticia=Noticias::findOne($codigo);
        $fotos=$noticia->getCodfos()->all();
        
        
        $dataProvider = new ActiveDataProvider([
    'query' => $consulta,
   
]);
        return $this->render("listar",[
            "dataProvider" => $dataProvider,
            "noticia" => $noticia,
            "fotos" => $fotos,
            
            ]);
    }
    
    

    
    
    public function actionEditarcomentario($codigo) {
        /*
         * Cargar los datos del comentarios desde la base de datos
         */
        
        $comentario= Comentarios::findOne($codigo);
        /*
         * quiero saber si he pulsado el boton editar desde el listado de comentarios 
         * o al boton modificar del formulario de actualizacion del comentario.
         */
        if($this->request->isPost){
            
            
            // aqui llego si he pulsado el boton de editar en el formulario del comentario
        
        
        if ($comentario->load(Yii::$app->request->post())) { //cargo los datos del formulario en el modelo
        $comentario->cod_noticias=$codigo;
        if ($comentario->save()) {
           
            // aqui solo llega si he plsado el boton de editar comnetario en el listado de comentario
            
            return $this->redirect(["vercomentarios","codigo"=>$comentario->cod_noticias]);
        }
    }
    
        }
        
        return $this->render('formulario', [
        'model' => $comentario,
        ]);
    }
    
    
    public function actionEliminarcomentario($codigo) {
        $comentario= Comentarios::findOne($codigo);
        
        
        //obtengo la noticia del comentario a eliminar.
       $cod_noticias=$comentario->cod_noticias;
       
        //eliminar el comentario
        $comentario->delete();
                return $this->redirect(["vercomentarios","codigo"=>$cod_noticias]);
    }
    
    
    
    /**
     * Login action.
     *
     * @return Response|string
     */
    

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionNuevanoticia(){
        
        $model= new Noticias; // modelo para almacenar los datos; Aqui se pregunta si vengo del formulario;
        
        
        
         if ($model->load(Yii::$app->request->post())) {
             
        
        if ($model->save()) { //intentado grabar lso datos en la tabla
            // form inputs are valid, do something here
            return $this->redirect(["site/index"]); //cargo la pagina principal
        }
    }
 //cargo el formulario porque es la primera vex que llamas a la accion o porque hay erroes en los fallos
    return $this->render('formulario_1', [
        'model' => $model,
    ]);
}
public function actionEditarnoticia($codigo) {
        /*
         * Cargar los datos del comentarios desde la base de datos
         */
        
        $noticia= Noticias::findOne($codigo);
        /*
         * quiero saber si he pulsado el boton editar desde el listado de comentarios 
         * o al boton modificar del formulario de actualizacion del comentario.
         */
        if($this->request->isPost){
            
            
            // aqui llego si he pulsado el boton de editar en el formulario del comentario
        
        
        if ($noticia->load(Yii::$app->request->post())) { //cargo los datos del formulario en el modelo
       
        if ($noticia->save()) {
           
            // aqui solo llega si he pulsado el boton de editar comnetario en el listado de comentario
            
            return $this->redirect(["index"]);
         }
        }
    
        }
        
        return $this->render('editarnoticia', [
        'model' => $noticia,
        ]);
    }
    
    public function actionEliminarnoticia($codigo) {
        $noticia= Noticias::findOne($codigo);
        
        
        //obtengo la noticia del comentario a eliminar.
      
       
        //eliminar el comentario
        $noticia->delete();
                return $this->redirect(["/site/index"]);
    }
         
    public function actionConfirmareliminarnoticia($codigo) {
        $noticia= Noticias::findOne($codigo);
        
        
        //obtengo la noticia del comentario a eliminar.
      
       
        //eliminar el comentario
        
                return $this->render("ver",["model"=> $noticia
                        ]);
    }
    
    /*
     * Metodo para añadir imagenes a la base de datos.
     * añade la imagen a la carpeta web/imgs
     * Y en la base de datos colocamos de la imagen.
     */
    
    public function actionFotos() {
        
        $model= new Fotos();
        
        // comprobadno si vengo del formulario
        if ($model->load(Yii::$app->request->post())) {
            $model->nombre=UploadedFile::getInstance($model, 'nombre');
            
        if ($model->save()) { //intentado grabar lso datos en la tabla
            // form inputs are valid, do something here
            return $this->redirect(["site/fotos"]); //cargo la pagina principal
        }
    }
// cargo el formulario la primera vez o cuando hay errores.
        return $this->render("fotos",[
            "model"=> $model
        ]);
        
    }
    
//    /*public function actionEditarfoto($codigo) {
//        /*
//         * Cargar los datos del comentarios desde la base de datos
//         */
//        
//        $foto= Noticias::findOne($codigo);
//        
//        /*
//         * quiero saber si he pulsado el boton editar desde el listado de comentarios 
//         * o al boton modificar del formulario de actualizacion del comentario.
//         */
//        if($this->request->isPost){
//            
//            
//            // aqui llego si he pulsado el boton de editar en el formulario del comentario
//        
//        
//        if ($foto->load(Yii::$app->request->post())) { //cargo los datos del formulario en el modelo
//       
//        if ($foto->save()) {
//           
//            // aqui solo llega si he pulsado el boton de editar comnetario en el listado de comentario
//            
//            return $this->redirect(["index"]);
//         }
//        }
//    
//        }
//        $model= new Fotos();
//        
//        // comprobadno si vengo del formulario
//        if ($model->load(Yii::$app->request->post())) {
//            $model->nombre=UploadedFile::getInstance($model, 'nombre');
//            
//        if ($model->save()) { //intentado grabar lso datos en la tabla
//            // form inputs are valid, do something here
//            return $this->redirect(["site/fotos"]); //cargo la pagina principal
//        }
//    }
//        return $this->render('editarnoticia', [
//        'model' => $foto,
//        ]);
//    }
//    */
    public function actionConfirmareliminarfoto($codigo) {
        $foto= Noticias::findOne($codigo);
        
        
        //obtengo la noticia del comentario a eliminar.
      
       
        //eliminar el comentario
        
                return $this->render("ver",["model"=> $foto
                        ]);
    }
    
    public function actionListarfotos(){
        $activeDataProvider=new ActiveDataProvider([
            "query" => Fotos::find(), 
        ]);
        
        
        return $this->render("listarfotos",[
            "dataProvider" => $activeDataProvider
            
        ]);
        
    }
    
    public function actionNoticiasfotos()
{
        
        //este model me permite almacenar todos los datos del formulario.
    $model = new NoticiasFotos();

    // compruebo si vengo del formulario
    if ($model->load(Yii::$app->request->post())) {
    
        $correcto=[];// array con la simagenes que pueda añadir a la noticia
        //logica de almacenamiento
        foreach ($model->codfo as $foto){
            //creo un modelo para cada uno de los registros fotos;
            
        }
                $modelo=new NoticiasFotos();
                $modelo->cofnoti=$model->cofnoti;
                $modelo->codfo=$foto;
                
                
                if($modelo->save()){
                    // si puedo almacenar el registro 
                // meto en el array correcto el nombre de la foto asociada a la noticia
                    $correcto[]=Fotos::findOne($foto)->nombre;
                }
            
            
            // en caso de que no pueda almacenar ninguno de los registros 
            //porque todas las fotos estaban ya en la noticia.
            //coloco un error
            if (count($correcto)>0) {
                
                // vista donde me muestra las fotos añadidas 
                return $this->render('salidanoticiafotos',[
                    'correctas'=>$correcto,
                ]);
            }else{
                
                //añadir un error al modelo en el capo cod_fptp
                $model->addError("cod_foto","todas las fotos estan ya en la noticia");
            }
        }
    return $this->render('noticiasfotos', [
        'model' => $model,
    ]);
    
    }
    
    public function actionEditarfoto($codigo){
        $model= Fotos::findOne($codigo);
        
        // comprobadno si vengo del formulario
        if ($model->load(Yii::$app->request->post())) {
            $model->nombre=UploadedFile::getInstance($model, 'nombre');
            
        if ($model->save()) { //intentado grabar lso datos en la tabla
            // form inputs are valid, do something here
            return $this->redirect(["site/fotos"]); //cargo la pagina principal
        }
    }
        
        
    }
    
    public function actionEliminarfoto($codigo){
    $model = Foto::findOne($codigo);
    $model->delete();
    return$this->redirect(['site/listarfotos']);
    
}

}