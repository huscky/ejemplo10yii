<?php
use yii\grid\GridView;
use yii\helpers\Html;
use yii\bootstrap4\Carousel;
?>
<h1><?=$noticia->titulo ?></h1>
<div><?=$noticia->texto ?></div>
<h2 class="mt-5 mb-auto">Comentarios de la noticia</h2>

  <?php
        
        //sacar las fotos de esa noticia
     
            $elementos=[];
            foreach ($fotos as $foto) {
               $elementos[]=Html::img("@web/imgs/" . $foto->nombre);
                
            }
        
        ?>
        <?=
        
         Carousel::widget([
          
         'items'=> $elementos,
             'options'=>[
            'class'=>'mx-auto col-lg-4'
         ],
            'controls' => ['<i class="fas fa-arrow-left fa-3x"></i>','<i class="fas fa-arrow-right fa-3x"></i>']

            
        
         ]);
        
        ?>
       
<?php
echo GridView::widget([
    'dataProvider' => $dataProvider,
    "layout"=>"{items}",
    "columns" =>[
        "texto",
        "fecha",
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{editar} {eliminar}',
            'buttons' => [
                'editar' => function ($url,$model) {
                    return Html::a('<i class="fas fa-edit"></i>editar',// logo de boton
                        ['site/editarcomentario',"codigo"=>$model->codigo],
                            );
                       
                },
                'eliminar' => function ($url,$model) {
                    return Html::a('<i class="far fa-trash-alt"></i> eliminar',// logo de boton
                            ['site/eliminarcomentario',"codigo"=>$model->codigo],
                            [
                                'data'=>[
                                    'confirm' => '¿Seguro que quieres borrar?',
                                    'method' => 'post',
                                    
                                ]
                                
                            ]);
                },
                        //("texto",["controlador/accion","id=1"])
	        ],
        ],
        
        
    ]

]);
echo "mostar los comentarios";
?>