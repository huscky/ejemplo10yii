<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Fotos */
/* @var $form ActiveForm */
?>
<div class="site-fotos">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php
    //tendre que colocar la foto antigua cuando venga de editar foto
    ?>
    
    

        <?= $form->field($model, 'nombre')->fileInput() ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
