<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
        "model" => $model
        
        ]);

?>
<div>
//colocarl el boton para eliminar
<?php

echo Html::a("¿Estas seguro de eliminar la noticia?",
        ["site/eliminarnoticia","codigo"=>$model->codigo],
        ["class"=>"btn btn-danger col-lg-4"]);

?>
</div>