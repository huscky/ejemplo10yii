
<?php
use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    "layout"=>"{items}",
    "options" => ['class' =>'text-center'],
    "columns" =>[
        'codigo',
        [
            'label'=>'foto',
            'format'=>'raw',
            'value' => function($model){
                return Html::img('@web/imgs/' . $model->nombre,['class'=> 'mx-auto col-lg-4']);
                
            }
        ],

        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
            
            'buttons' => [
                'update' => function ($url,$model) {
                    return Html::a('<i class="fas fa-edit"></i>editar',// logo de boton
                        ['site/editarfoto',"codigo"=>$model->codigo],
                            );
                       
                },
                'delete' => function ($url,$model) {
                    return Html::a('<i class="far fa-trash-alt"></i> eliminar',// logo de boton
                            ['site/eliminarfoto',"codigo"=>$model->codigo],
                            [
                                //desplegable de borrar
                                'data'=>[
                                    'confirm' => '¿Seguro que quieres borrar?',
                                    'method' => 'post',
                                    
                                ]
                                
                            ]);
                },
                        //("texto",["controlador/accion","id=1"])
	        ],
         ]
        
        
]
]);
                
                ?>