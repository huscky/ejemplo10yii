<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NoticiasFotos */
/* @var $form ActiveForm */
?>
<div class="site-noticiasfotos">

    <?php $form = ActiveForm::begin(); ?>
    
<?php 
// meter todas las noticias en la variable para mostrarlas en el.
// desplegable
       $noticias= app\models\Noticias::find()->all();


$listData= yii\helpers\ArrayHelper::map($noticias,'codigo',function ($model){
    return $model->codigo . " - " . $model->titulo;// en el desplegable colocamos el codigo de la noticia y el titulo.
});

echo $form->field($model, 'cofnoti')->dropDownList(
        $listData,
        ['prompt'=>'seleciona una noticia']
        );
?>
    
     <?php //dropdawn
// almacenar en la variable todas las fotos de la tabla fotos
       $noticias= app\models\Fotos::find()->all();

//**opcion1*//
//**Mostrar solamente el codigo y el nombre de la imagen**//
       
       

// creando una variante con el codigo y el nombre de cada una de las fotos
//echo $form->field($model, 'codfo')->dropDownList(
//        $listData,
//        ['prompt'=>'seleciona una foto']
//        );
// creo una variable con las img de todas las fotos de la base de datos.

//fin opcion1//

//**opcion2**//
//Mostrar en un listado todas las iamgenes**//
$listData= yii\helpers\ArrayHelper::map($noticias,'codigo',function($model){
    return  HTML::img ("@web/imgs/" . $model->nombre,["class"=>'col-lg-10 img-fluid']);
    
});

// listar todas las imagenes en un listado de casillas de verificacion.
echo $form->field($model, 'codfo',['options'=>['class'=>'col-lg-3 p-0']])->checkboxList(
                $listData,
                ['prompt'=>'Selecciona una imagen','encode'=>false]
            );
//**fin de la opcion 2**//
?>
    

    
    
        
        <?= $form->field($model, 'visitas') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-noticiasfotos -->
