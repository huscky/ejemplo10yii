<?php



namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

class MostrarFotos extends Widget {
    //put your code here

    public $fotos;

    public function init()
    {
        parent::init();
       
    }

    public function run()
    {
        $salida='<div class="row">';
        foreach ($this->fotos as $foto){
    $salida.= Html::img("@web/imgs/$foto",['class'=>'col-lg-4']);
}
$salida.='</div>';


        return Html::decode($salida);
    }
}