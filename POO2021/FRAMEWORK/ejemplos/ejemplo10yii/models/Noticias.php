<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticias".
 *
 * @property int $codigo
 * @property string|null $titulo
 * @property string|null $texto
 * @property string|null $fecha
 *
 * @property Fotos[] $codfos
 * @property Comentarios[] $comentarios
 * @property NoticiasFotos[] $noticiasFotos
 */
class Noticias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'date','format'=>'php:d/m/Y'],
            [['titulo'], 'string', 'max' => 100],
            [['texto'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'titulo' => 'Titulo',
            'texto' => 'Texto',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[Codfos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodfos()
    {
        return $this->hasMany(Fotos::className(), ['codigo' => 'codfo'])->viaTable('noticias_fotos', ['cofnoti' => 'codigo']);
    }

    /**
     * Gets query for [[Comentarios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComentarios()
    {
        return $this->hasMany(Comentarios::className(), ['cod_noticias' => 'codigo']);
    }

    /**
     * Gets query for [[NoticiasFotos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNoticiasFotos()
    {
        return $this->hasMany(NoticiasFotos::className(), ['cofnoti' => 'codigo']);
    }
    
    
    /*
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
    
        
    }
    */
    /*
     * este metodo se ejecuta automaticamnete antes de insertar o modificar una noticia.
     */
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        
        if (!empty($this->fecha)){
        
        // este metodo me permite convertir un texto en una fecha valida para mi sistema.
    $this->fecha= \DateTime::createFromFormat("d/m/Y", $this->fecha)->format("Y/m/d");
    return true;
      
    }
    
    }
    // metodo despues de ejecutar un find (que es un select) select * from Noticias 
    // voya a colocar la fecha en formato dia mes año cuando muestre el registro.
    public function afterFind() {
        parent::afterFind();
        if (!empty($this->fecha)){
        $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d/m/Y');
        
        
    }
}
}  
