<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "noticias_fotos".
 *
 * @property int $codfo
 * @property int $cofnoti
 * @property int|null $visitas
 *
 * @property Fotos $codfo0
 * @property Noticias $cofnoti0
 */
class NoticiasFotos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'noticias_fotos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codfo', 'cofnoti'], 'required'],
            [['codfo', 'cofnoti', 'visitas'], 'integer'],
            [['codfo', 'cofnoti'], 'unique', 'targetAttribute' => ['codfo', 'cofnoti']],
            [['cofnoti'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::className(), 'targetAttribute' => ['cofnoti' => 'codigo']],
            [['codfo'], 'exist', 'skipOnError' => true, 'targetClass' => Fotos::className(), 'targetAttribute' => ['codfo' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codfo' => 'Codfo',
            'cofnoti' => 'Cofnoti',
            'visitas' => 'Visitas',
        ];
    }

    /**
     * Gets query for [[Codfo0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodfo0()
    {
        return $this->hasOne(Fotos::className(), ['codigo' => 'codfo']);
    }

    /**
     * Gets query for [[Cofnoti0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCofnoti0()
    {
        return $this->hasOne(Noticias::className(), ['codigo' => 'cofnoti']);
    }
}
