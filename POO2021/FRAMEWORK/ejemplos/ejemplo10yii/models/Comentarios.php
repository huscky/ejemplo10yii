<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comentarios".
 *
 * @property int $codigo
 * @property string|null $texto
 * @property string|null $fecha
 * @property int $cod_noticias
 *
 * @property Noticias $codNoticias
 */
class Comentarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comentarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['cod_noticias'], 'required'],
            [['cod_noticias'], 'integer'],
            [['texto'], 'string', 'max' => 1000],
            [['cod_noticias'], 'exist', 'skipOnError' => true, 'targetClass' => Noticias::className(), 'targetAttribute' => ['cod_noticias' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'texto' => 'Texto',
            'fecha' => 'Fecha',
            'cod_noticias' => 'Cod Noticias',
        ];
    }

    /**
     * Gets query for [[CodNoticias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodNoticias()
    {
        return $this->hasOne(Noticias::className(), ['codigo' => 'cod_noticias']);
    }
    
    
    
    public function getNoticiasFotos()
    {
        return $this->hasMany(NoticiasFotos::className(), ['cofnoti' => 'codigo']);
    }
    
     public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
     }
    
     
    public function beforeSave($insert) {
       
        
        $this->fecha= date("Y/m/d");
    return true;
      
    }
    
    public function afterFind() {
        parent::afterFind();
        //voy a colocar la fecha 
        
        if (!empty($this->fecha)){// si la fecha no esta vacia. 
        $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d/m/Y');
        
        }
    }
}
