<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "camion".
 *
 * @property int $Matricula
 * @property string|null $Potencia
 * @property string|null $modelo
 * @property string|null $Tipo
 */
class Camion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'camion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Matricula'], 'required'],
            [['Matricula'], 'integer'],
            [['Potencia', 'modelo', 'Tipo'], 'string', 'max' => 255],
            [['Matricula'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Matricula' => 'Matricula',
            'Potencia' => 'Potencia',
            'modelo' => 'Modelo',
            'Tipo' => 'Tipo',
        ];
    }
}
