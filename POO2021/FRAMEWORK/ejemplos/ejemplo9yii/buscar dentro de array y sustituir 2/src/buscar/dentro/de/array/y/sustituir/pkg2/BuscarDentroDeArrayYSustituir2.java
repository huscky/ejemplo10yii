/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buscar.dentro.de.array.y.sustituir.pkg2;


import java.util.Scanner;
import javax.swing.JOptionPane;
import java.lang.Math;
/**
 *
 * @author Programacion
 */
public class BuscarDentroDeArrayYSustituir2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Rehaced el ejercicio anterior
        //pero ahora pediremos además al usuario el número de elementos del array
        //y lo generaremos automáticamente con números entre 1 y 9
       int num, tamaño, contador=0;
        
        String medida = JOptionPane.showInputDialog("Indica el tamaño del array ");
        tamaño = Integer.parseInt(medida);
        //pedimos un numero al usuario para sustituirlo por cero en el array
        Scanner entrada = new Scanner(System.in);
        System.out.print("Indica el numero para quitarlo y preparate para cagarla fijo ");
        num = entrada.nextInt();
        
        int [] numeros  = new int [tamaño];
        int [] original = new int [tamaño];
        
        //lo primero es generar aleatorios entre 1 y 9 para rellenart el array
        for (int i = 0; i < original.length; i++) {
            original[i]= (int) (Math.random()*9+1);
            // compara el original con el num. y si coincide lo iguale a 0.
            if(original[i]==num){ numeros[i]=0;
                contador++;
            }else numeros[i]=original[i];

        }
        
        System.out.println("El array final es: ");
        for (int j = 0; j < numeros.length; j++) {
            System.out.print(numeros[j]+" ");
        }
        
        System.out.println();
        System.out.println("Se ha encontrado "+contador+" ocurrencias del numero "+num);
        
        
        System.out.println("El array original es: ");
        for (int l = 0; l < original.length; l++) {
            System.out.print(original[l]+" ");
        }
    }
}