/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Programacion
 */
public class Libro {
    
    
    //Atributos
    private String titulo;
    private String autor;
    private int  n_ejemplar;
    private int n_ejemplados_prestados;
    //Clase Libro
//Crea una clase llamada Libro que guarde la información de cada uno de los libros de una biblioteca. La clase debe guardar el título del libro, autor, número de ejemplares del libro y número de ejemplares prestados. La clase contendrá los siguientes métodos:
//Constructor por defecto.
//Constructor con todos los parámetros.
//•	Métodos Setters/getters

    
    
    
    public  Libro (){
    
    
    }
    
    

    public Libro(String titulo, String autor, int n_ejemplar, int n_ejemplados_prestados) {
        this.titulo = titulo;
        this.autor = autor;
        this.n_ejemplar = n_ejemplar;
        this.n_ejemplados_prestados = n_ejemplados_prestados;
    }

    // get saca datos //set mete datos
    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getN_ejemplar() {
        return n_ejemplar;
    }

    public void setN_ejemplar(int n_ejemplar) {
        this.n_ejemplar = n_ejemplar;
    }

    public int getN_ejemplados_prestados() {
        return n_ejemplados_prestados;
    }

    public void setN_ejemplados_prestados(int n_ejemplados_prestados) {
        this.n_ejemplados_prestados = n_ejemplados_prestados;
    }
    
    // Método préstamo que incremente el atributo correspondiente cada vez que se realice un préstamo del libro. 
    //No se podrán prestar libros de los que no queden ejemplares disponibles para prestar. 
    //Devuelve true si se ha podido realizar la operación y false en caso contrario.
    
    public  boolean  prestamo(){
    // si numero de ejemplare es menor o igual a los ejemplares prestados devolvemos falso.

    if (this.n_ejemplar<=this.n_ejemplados_prestados){
        return false;
    }else{    //y si no se suma prestados 
        n_ejemplados_prestados++;
        return true; // devolvemos verdadero
        
                
   
    }
    }
//nuevo objeto llamada colmena 
    
    
    
    //Método devolución que decremente el atributo correspondiente cuando se produzca la devolución de un libro. 
    //No se podrán devolver libros que no se hayan prestado.
    //Devuelve true si se ha podido realizar la operación y false en caso contrario.
    
    public boolean devolucion () {
    
    if (this.n_ejemplados_prestados>0){
        n_ejemplados_prestados--;
        return true;
    }else {
    
    return false;
    
    }
    }

    public void mostrarDatos(){
        System.out.println("el libro"+this.titulo+"el autor"+this.autor+"tiene" +this.n_ejemplar+"de los cueles estan prestados"+this.n_ejemplados_prestados);
        
    }
       
}


    
