<?php

use yii\grid\GridView;
use yii\helpers\Html;

echo GridView::widget([
    "dataProvider" => $dataProvider,
    "columns"=>[
        'id',
        'nombre',
        'categorias',
        'foto',
        [
          'label'=>'Portada',
           'format'=>'raw',
            'value'=>function($data){
            $url ="@web/imgs/$data->foto";
            return Html::img($url,['style'=>'width:200px']);
    
    
            }
            
            
            
        ],
        'precio',
        'descripcion',
        'oferta',        
                
    ]
    
    
    
]);
