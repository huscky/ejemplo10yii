


<?php
use yii\helpers\Html;
?>
<div class="row">
    <div class="col-lg-5">
    <?php
    echo Html::img("@web/imgs/$model->foto",[
    "class" =>"card-img-top"
    ]);
    
    ?>
    </div>
    <div class="col-lg-7">
        <h2><?= $model->nombre ?><br></h2>
        <div class="bg-warning rounded p-2">Foto:</div>
        <div class="p-1"><?= $model->foto ?></div>
        <div class="bg-warning rounded p-2">Descripción:</div>
        <div class="p-1"><?= $model->descripcion ?></div>
        <div class="bg-warning rounded p-2">Precio:</div>
        <div class="p-1"><?= $model->precio ?></div>
           
    </div>
    
</div>

