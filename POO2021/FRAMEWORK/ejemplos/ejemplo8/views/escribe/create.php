<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\escribe */

$this->title = 'Create Escribe';
$this->params['breadcrumbs'][] = ['label' => 'Escribes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="escribe-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
