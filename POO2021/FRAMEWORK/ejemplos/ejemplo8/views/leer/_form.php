<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\leer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="leer-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'autor')->dropDownList($listadoAutores); ?>

    <?= $form->field($model, 'libro')->dropDownList($listadoLibros)?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
