<?php

namespace app\controllers;

use app\models\leer;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LeerController implements the CRUD actions for leer model.
 */
class LeerController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all leer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => leer::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'autor' => SORT_DESC,
                    'libro' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single leer model.
     * @param int $autor Autor
     * @param int $libro Libro
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($autor, $libro)
    {
        return $this->render('view', [
            'model' => $this->findModel($autor, $libro),
        ]);
    }

    /**
     * Creates a new leer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new leer();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'autor' => $model->autor, 'libro' => $model->libro]);
            }
        } else {
            $model->loadDefaultValues();
        }
        
$autores = \app\models\Autores::find()->all();
$libros = \app\models\Libros::find()->all();

$listadoAutores= \yii\helpers\ArrayHelper::map($autores, 'id', 'nombre');
$listadoLibros = \yii\helpers\ArrayHelper::map($libros, 'id', 'nombre');

        return $this->render('create', [
            'model' => $model,
            
            'listadoAutores'=>$listadoAutores,
            'listadoLibros' => $listadoLibros,
        ]);
    }

    /**
     * Updates an existing leer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $autor Autor
     * @param int $libro Libro
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($autor, $libro)
    {
        $model = $this->findModel($autor, $libro);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'autor' => $model->autor, 'libro' => $model->libro]);
        }
$autores = \app\models\Autores::find()->all();
$libros = \app\models\Libros::find()->all();

$listadoAutores= \yii\helpers\ArrayHelper::map($autores, 'id', 'nombre');
$listadoLibros = \yii\helpers\ArrayHelper::map($libros, 'id', 'nombre');
        return $this->render('update', [
            'model' => $model,
            
             'listadoAutores'=>$listadoAutores,
            'listadoLibros' => $listadoLibros,
        ]);
    }

    /**
     * Deletes an existing leer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $autor Autor
     * @param int $libro Libro
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($autor, $libro)
    {
        $this->findModel($autor, $libro)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the leer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $autor Autor
     * @param int $libro Libro
     * @return leer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($autor, $libro)
    {
        if (($model = leer::findOne(['autor' => $autor, 'libro' => $libro])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
