<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'Concesionario Leon';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Congratulations!</h1>

       <?= Html::img('@web/imgs/concesionario.jpg', [ "class" => "img-fluid"] )?>
  
            </div>
        </div>

    </div>
</div>
