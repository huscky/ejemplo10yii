<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Notas */
/* @var $form ActiveForm */
?>
<div class="site-formulario">

    <?php $form = ActiveForm::begin(); ?>

        
        <?= $form->field($model, 'titulo') ?>
        <?= $form->field($model, 'descripcion')->textarea(["cols"=>40,"rows"=>10]) ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-formulario -->
