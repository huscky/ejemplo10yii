<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Numeros;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string

          */
    
    public function actionSumar(){
        $model=new Numeros();
        $model->tipo="sumar";
        
          
         $datos=Yii::$app->request->post();//recoger lo que manda el formulario
         if ($model->load($datos) && $model->validate()){ // comprueba si le llega algo del formulario y c umple las rules
             $resultado=$model->operacion();
             return $this->render("resultado",[
                 "operacion " => $model->tipo
         
         ]);
         }  
         //$model = new \app\models\Sumar();
       
                return $this->render('numeros',[
                    "model" =>$model,
                    "operacion" => "Sumar"
                
             ]);
                     
                     
             
         }
        
    
    public function actionRestar() {
        
     $model=new Numeros();
     $model->tipo="restar";   
        
          
         $datos=Yii::$app->request->post();//recoger lo que manda el formulario
         if ($model->load($datos) && $model->validate()){ // comprueba si le llega algo del formulario y c umple las rules
             $resultado=$model->operacion();
             return $this->render("resultado",[
                 "operacion" => "resta"
         
         ]);
         }  
         //$model = new \app\models\Sumar();
       
                return $this->render('numeros',[
                    "model" =>$model,
                    "operacion" => "Restar"
                    
                
             ]);
                     
                     
                     
             
         }
    
    public function actionMultiplicar(){
        
    $model=new Numeros();
    $model->tipo="multiplicar";    
        
          
         $datos=Yii::$app->request->post();//recoger lo que manda el formulario
         if ($model->load($datos) && $model->validate()){ // comprueba si le llega algo del formulario y c umple las rules
             $resultado=$model->operacion();
             return $this->render("resultado",[
                 "operacion" => "multiplicaciones"
         
         ]);
         }  
         //$model = new \app\models\Sumar();
       
                return $this->render('numeros',[
                    "model" =>$model,
                    "operacion" => "multiplicar"
                    
                
             ]);
                     
                     
                     
             
         }
    public function actionDividir() {
   $model=new Numeros();
     $model->tipo="dividir";   
        
          
         $datos=Yii::$app->request->post();//recoger lo que manda el formulario
         if ($model->load($datos) && $model->validate()){ // comprueba si le llega algo del formulario y c umple las rules
             $resultado=$model->operacion();
             return $this->render("resultado",[
                 "resultado" => $resultado,
                     "operacion"=>"division"
         ]);
         }  
         //$model = new \app\models\Sumar();
       
                return $this->render('numeros',[
                    "model" =>$model,
                    "operacion" => "dividir"
                    
                
             ]);
                     
                     
                     
             
         }
         
         public function actionOperar(){
             $model=new Numeros();
             $model->tipo="dividir";   
        
 
         $datos=Yii::$app->request->post();//recoger lo que manda el formulario
         if ($model->load($datos) && $model->validate()){ // comprueba si le llega algo del formulario y c umple las rules
             //realiza todas las operaciones
             
             $model->operarTodo();
         
             return $this->render("mostrarTodo",[
                 "model" => $model
         
         ]);
             
             
         }
         
         return $this->render('numeros',[
                 "model" => $model,
                "operacion" =>"Calcular"
         ]);
         }
}