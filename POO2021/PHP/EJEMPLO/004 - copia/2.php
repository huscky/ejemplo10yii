<?php

/**
 * Funcion que suma todos los elementso del array
 * @param array $numeros array con los numeros a sumar.
 * @return int la suma de todos los numeros del array
 */
function suma ($numeros){
    $resultado=0;
   foreach($numeros as $valor){
       
       $resultado+=$valor;
       
   }
    
   return $resultado;
}
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $edades=[28,29,32,19,24,7];
        echo suma($edades);
        ?>
    </body>
</html>
