<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $fotos = [
            'bici', 'bobina', 'caballito', 'cascos', 'comida', 'edificio', 'lunes'
        ];
        $numerofotos = count($fotos);
        const TOTA = 10;

        FOR ($contador = 0; $contador < TOTA; $contador++) {

            $indice = mt_rand(0, $numerofotos - 1);
            $foto = $fotos[$indice];
            ?>

            <img src="imgs/<?= $foto ?>.jpg" alt="foto bici"/>
            <?php
        }
        ?>
    </body>
</html>
