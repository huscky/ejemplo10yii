<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<?php


function operacionconretorno ($a, $b){
    $c=[
        
        "suma"=> $a+$b,
        "resta" => $a-$b,
        "producto" => $a*$b
        
    ];
    
    //en 3 intrucciones
    
    //$c["suma"]=$a+$b,
   //$c["resta"]=$a-$b,
    //$c[producto]=$a*$b;
    return $c; /*devuelve la suma*/
    
    
    
}

function operacionreferencia($a,$b,&$c){
  //  $c=$a+$b;
    $c=[
        
        "suma"=> $a+$b,
        "resta" => $a-$b,
        "producto" => $a*$b
        
    ];
    
}
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $numero1=20;
        $numero2=10;
        $resultado=[
        
        ];
        $resultado=operacionconretorno($numero1, $numero2);
        var_dump($resultado);
        
        $resultado=0;
        operacionreferencia($numero1, /*argumento por valor*/ $numero2,/*argumento por valor*/ $resultado/*argumento por referencia lo que se cambia en $c se cambia en $resultado*/);
        var_dump($resultado);
        ?>
    </body>
</html>
