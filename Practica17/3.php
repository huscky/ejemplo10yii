<?php
 class Usuario {
     
     
     //propiedades
     var $nombre = "defecto";
     private $edad;
     protected $telefono;
     private $apellidos = "defecto";
     private $nombreCompleto;
     
     
     public function getNombreCompleto() {
         return $this->nombreCompleto;
     }
     
     public function getApellidos() {
         return $this->apellidos;
 }
 public function setApellidos($apellidos) {
     $this->apellidos = $apellidos;
     $this->concatenar();
 }


 public function getNombre() {
     return $this->nombre;
 }
public function getEdad(){
    
    return $this->edad;
}

public function getTelefono (){
    
    return $this->telefono;
}

public function setNombre ($nombre){
    $this->nombre = $nombre;
    $this->concatenar();
}

public function setEdad($edad){
    $this->edad = $edad;
    
}

public function setTelefono($telefono) {
    $this->telefono =$telefono;
}

//metodo privado

/**
 * asignando a la propiedad nombre completo el valor del nombre y los apellidos.
 */

private function concatenar(){
    $this->nombreCompleto = $this->nombre . " " . $this->apellidos;
   
    
    
}
 }

/** crear el objeto*/


$persona = new Usuario ();
    echo $persona->nombre; // leo la propiedad nombre ya que es publica ("defecti")
  $persona->setEdad(51); // asignando una edad con setter
  $persona->setTelefono("232323"); // asigno un telefono en el setter.
  
  $persona->setApellidos("Vazquez rodriguez"); //asigno los apellidos con el setter
  var_dump($persona);
  $persona->nombre="Ramon"; // asigno un nombre al objeto con la propiedad (al utilizar la propiedad no actualiza nombre completo)
  var_dump($persona);
  $persona->setNombre("Ramon"); // asigno el nombre al objeto con el setter
  var_dump($persona);
?>    
    
<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // put your code here
        ?>
    </body>
</html>
