<!DOCTYPE html>
<?php
include_once './ejercicio6/Vehiculo.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        
        //instacion un objeto de tipo vehiculo
        //ademas incializo el vehiculo.
       $ford=new Vehiculo ("DHH2323", "rojo", false);
       // para llamar a un metodo estico puedo utilizar
       $ford->mensaje(); // forma 1(no se aconseja)
       Vehiculo::mensaje (); //forma 2 recomendada; nombre del metodo.
       $ford::mensaje(); //forma 3 (no os acosejo)
       
       //muestro todas las propiedad del nuevo obejto
       var_dump($ford);
       
       //no puedo acceder a una propiedad estatico de esta forma
       //echo $ford->ruedas;
       //para acceder a una propiedad estatica
       vehiculo::$ruedas;
       
       
       $alumnos=new Alumnos();
       
       $alumnos->nombre="pepe"
       $alumnos->save();
       
       
       $alumnos->findAll();
       
       
       
       echo $ford->ruedas ();
       echo $ford ::$ruedas; // asi se puede acceder a una propiedad 
       
       echo Vehiculo::$ruedas;
       vehiculo::$ruedas=6; //escribimmos la propiedad
       echo $ford->matricula;
       
       echo Vehiculo::$ruedas;
       echo $ford-> ruedas;
       $ford::encender();
       
       vehiculo::enceder, // esta llamada es correcto pero el metodo esta mal creado
               
          /**
           * Cconcretando
           * para acceder a propiedad estaticas
           * NombreClase::$Nombrepropiedad
           * $Nombreobjeto::$Nombrepropiedad
           */  
               
               
           /**
            * concretando
            * para llamar a metodos estaticos
            * NombreClse::Nombre Metodo();
            * $NombreObjeto::NombreMetodo();
            */    
               
           /**
            * Concretando
            * si quiero acceder a los elementos no estaticos
            * 
            * $Nombreobjeto->NombrePropiedad;
            * $Nombre->Nombremetodo();
            */    
               
        ?>
    </body>
</html>
