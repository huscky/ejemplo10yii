<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of diseño
 *
 * @author Programacion
 */
class Vehiculo {
    public $matricula;
    private $color;
    protected $encendido;
    
    public function encender(){
        $this->encendido = true;
        echo 'Vehiculo encendido <br/>';
          
    }
    
    public function apagar() {
        $this->encendido = false;
        echo 'Vehiculo apagado <br />';
    }
}
?>


<?php

class Camion extends Vehiculo {
    
    private $carga;
    
    public function cargar($cantidad_a_cargar) {
        $this->carga = $cantidad_a_cargar;
        echo 'Se ha cargado:' . $cantidad_a_cargar . '<br/>';
        
    }
    public function verificar_encendido() {
        if($this->encendido == True){
            echo 'Camion encendido <br />';
            
        }else{
            echo 'Camion apagado <br/>';
            
            
        }
        
    }
    
}

?>


<?php

class Autobus extends Vehiculo{
    
    private $pasajeros;
    
    public function subir_pasajeros($cantidad_pasajeros){
        $this->pasajeros = $cantidad_pasajeros;
        echo 'Se han subido' .$cantidad_pasajeros.'pasajeros <br />';
        
        
    }
    
    public function verificar_encendido(){
        if ($this->encendido == true){
            echo 'Autobus encendido <br />';
             
        }else{
            echo 'Autobus apagado <br />';
            
            
        }
        
        
    }
}
?>
