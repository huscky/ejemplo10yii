<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 1 de la hoja de Ejercicios 1 de PHP</title>
    </head>
    <body>
    En este ejercicio vamos a escribir un texto en la pagina web a través de PHP
    <br>
        <?php
       //Esto es un comentario
        
        echo "Este texto esta escrito desde el script de PHP.";
        
    /* Esto es otro comentario
    pero con la diferencia de que es de varias líneas*/
        
        echo "Este texto tambien se escribe desde el script de php.";
        ?>
    
    <br>
    Esto esta escrito en HTML normal
    <br>
    <?php
    ###################################################################
    #############  Este comentario es de una sola linea ###############  
    ###################################################################

    # En una página podemos poner tantos scripts de php como se desee

    print("Esta es otra forma d eescribir cosas en la web");

    ?>
   
    </body>
</html>
