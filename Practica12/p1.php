<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        $anchuraDados=140;
        $alturaDados=140;
        
        ?>
        <style type="text/css">
            img{
                width: <?=$anchuraDados?>px;
                height: <?=$alturaDados?>px;
            }
            .dados{
                width:<?=$anchuraDados*2?>px;
            }
            
            .total{
                margin:10px;
            }
            
            span{
                border: black 2px solid; 
                padding: 10px; 
                font-size: 300%;
            }
        </style>
    </head>
    <body>
        <?php
        $dado1 = rand(1, 6);
        $dado2 = rand(1, 6);
        ?>
        <div class="dados">
            <img src="imgs/<?= $dado1 ?>.svg" alt="dado1"/>
            <img src="imgs/<?= $dado2 ?>.svg" alt="dado2"/>
        </div>
        <div class="total">Total: <span> <?=($dado1 + $dado2)?></span></div>
    </body>
</html>
